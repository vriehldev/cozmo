import random
import cozmo
from cozmo.objects import LightCube1Id, LightCube2Id, LightCube3Id


# Integer : le choix du joueur et le choix du robot
choixJoueur = 0
choixRobot = 0

# booleen : indique si le joueur a joué
joueurAJoue = False

# booleen : indique si quelqu'un a gagné la partie
qqunAGagne = False

# Cubes
cube1 = 0
cube2 = 0
cube3 = 0


def shifumi(robot: cozmo.robot.Robot):
    global cube1
    global cube2
    global cube3

    global joueurAJoue
    global qqunAGagne

    # On récupère les cubes disponibles dans l'environnement du robot
    cube1 = robot.world.get_light_cube(LightCube1Id)  # Cube rouge --> Ciseau
    cube2 = robot.world.get_light_cube(LightCube2Id)  # Cube bleu  --> Pierre
    cube3 = robot.world.get_light_cube(LightCube3Id)  # Cube vert  --> Feuille

    # On ajoute les couleurs aux cubes
    if cube1 is not None:
        cube1.set_lights(cozmo.lights.red_light)

    if cube2 is not None:
        cube2.set_lights(cozmo.lights.blue_light)

    if cube3 is not None:
        cube3.set_lights(cozmo.lights.green_light)

    robot.say_text(
        "On va jouer à Pierre Feuille Ciseau. Tappe sur un des cubes pour faire ton choix.").wait_for_completed()
    robot.say_text("Le cube rouge est Ciseau, le cube bleu est Pierre, le cube vert est Feuille.").wait_for_completed()

    while (not qqunAGagne):
        handler = robot.add_event_handler(cozmo.objects.EvtObjectTapped, on_cube_tapped)
        if (joueurAJoue):
            choix_robot(robot)
            resultat(robot)


# Evénement se produisant lorsque le joueur tappe un cube
def on_cube_tapped(event, *, obj, tap_count, tap_duration, **kw):
    global cube1
    global cube2
    global cube3

    global choixJoueur

    # On récupère la réponse du joueur grâce au cube qu'il a tappé
    if obj == cube1:
        choixJoueur = 1
    elif obj == cube2:
        choixJoueur = 2
    elif obj == cube3:
        choixJoueur = 3

    # On met le booleen tapote à true
    tapote = True


def choix_robot(robot: cozmo.robot.Robot):
    global choixRobot
    proposition = ""
    # Il génère un nombre entre 0 et 2
    choixRobot = random.randint(1, 3)

    if (choixRobot == 1):
        proposition = "Ciseau"
    elif (choixRobot == 2):
        proposition = "Pierre"
    elif (choixRobot == 3):
        proposition = "Feuille"
    robot.say_text(str(proposition)).wait_for_completed()


def resultat(robot: cozmo.robot.Robot):
    global choixJoueur
    global choixRobot

    global joueurAJoue
    global qqunAGagne

    if (choixJoueur != 0 and choixRobot != 0):  # Si le joueur et le robot ont fait leur choix
        res = choixJoueur - choixRobot

        if (
                res == 1 or res == -2):  # Si joueur:feuille/robot:pierre ou joueur:pierre/robot:ciseau ou joueur:ciseau/robot:feuille
            # Le joueur a gagné
            robot.say_text("Oh non, j'ai perdu !").wait_for_completed()
            qqunAGagne = True
        elif (
                res == 2 or res == -1):  # Si joueur:pierre/robot:feuille ou joueur:ciseau/robot:pierre ou joueur:feuille/robot:ciseau
            # Le robot a gagné
            robot.play_anim(name="anim_poked_giggle").wait_for_completed()
            robot.say_text("Ouais ! J'ai gagné").wait_for_completed()
            qqunAGagne = True
        elif (res == 0):  # Si robot et joueur ont joué la même chose
            robot.say_text("Égailté ! On continue jusqu'à avoir un ganant !").wait_for_completed()

        choixJoueur = 0
        choixRobot = 0
        joueurAJoue = False
