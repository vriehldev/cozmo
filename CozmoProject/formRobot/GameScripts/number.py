import cozmo
from cozmo.objects import LightCube1Id, LightCube2Id, LightCube3Id

import random

# Booléen : False jusqu'à ce que le nombre soit trouvé
trouve = False
# Booléen : passe à True quand la personne tape sur un cube
tapote = False
# Bornes pour que le robot définisse une proposition
borneInf = 0
borneSup = 100
# Proposition du robot
proposition = 0
# Cubes
cube1 = 0
cube2 = 0
cube3 = 0
# Cube sur lequel la personne a appuyé
cubeTapote = 0


def cozmo_ask_number(robot: cozmo.robot.Robot):
    global proposition
    # Il génère un nombre entre la borne inférieure et la borne supérieure
    proposition = random.randint(borneInf, borneSup)
    # Il dit sa proposition
    robot.say_text(str(proposition)).wait_for_completed()


def cozmo_get_respond(robot: cozmo.robot.Robot, answer):
    global borneInf
    global borneSup
    randomNumToSay = 0

    if answer != None:
        # S'il tape sur le cube1 (rouge)
        if answer == 0:
            # Il remplace la borne supérieure par proposition - 1
            borneSup = proposition - 1
            # Nouvelle proposition du robot
            cozmo_ask_number(robot)
        # S'il tape sur le cube2 (bleu)
        elif answer == 1:
            # Il remplace la borne inférieure par proposition + 1
            borneInf = proposition + 1
            # Nouvelle proposition du robot
            cozmo_ask_number(robot)
        # S'il tape sur le cube3 (vert)
        elif answer == 2:
            # Le robot a gagné
            on_game_won(robot)
        # Sinon il y a une erreur
        else:
            cozmo_error(robot)

# Erreur


def cozmo_error(robot: cozmo.robot.Robot):
    robot.say_text("Erreur").wait_for_completed()

# Victoire


def on_game_won(robot: cozmo.robot.Robot):
    global trouve
    # Le robot fait une danse de la victoire
    robot.play_anim(name="anim_poked_giggle").wait_for_completed()
    # Le robot dit qu'il a gagné
    robot.say_text("J'ai gagné").wait_for_completed()
    # Le booléen trouve passe à true
    trouve = True


def findNumber(robot: cozmo.robot.Robot):
    global trouve
    global tapote
    global cube1
    global cube2
    global cube3
    global cubeTapote

    # On récupère les cubes disponibles dans l'environnement du robot
    # Cube rouge (nombre à trouver est inférieur)
    cube1 = robot.world.get_light_cube(LightCube1Id)
    # Cube bleu (nombre à trouver est supérieur)
    cube2 = robot.world.get_light_cube(LightCube2Id)
    cube3 = robot.world.get_light_cube(
        LightCube3Id)  # Cube vert (nombre trouvé)

    # On ajoute les couleurs au robot
    if cube1 is not None:
        cube1.set_lights(cozmo.lights.red_light)

    if cube2 is not None:
        cube2.set_lights(cozmo.lights.blue_light)

    if cube3 is not None:
        cube3.set_lights(cozmo.lights.green_light)

    # Le robot fait une première proposition
    cozmo_ask_number(robot)

    # Jusqu'à ce que le robot trouve le nombre :
    while(not trouve):
        # Lorsqu'on tape sur un cube
        handler = robot.add_event_handler(
            cozmo.objects.EvtObjectTapped, on_cube_tapped)
        if(tapote):
            # Le robot fait une proposition
            cozmo_get_respond(robot, cubeTapote)
            tapote = False


def on_cube_tapped(event, *, obj, tap_count, tap_duration, **kw):
    global cube1
    global cube2
    global cube3
    global cubeTapote
    global tapote

    # On récupère le cube sur lequel on a tapé
    if obj == cube1:
        cubeTapote = 0
    elif obj == cube2:
        cubeTapote = 1
    elif obj == cube3:
        cubeTapote = 2
    else:
        cubeTapote = -1

    # On met le booleen tapote à true
    tapote = True