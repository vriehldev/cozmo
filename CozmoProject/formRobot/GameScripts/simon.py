import time
import random

from formRobot.CustomLibrary.libCozmo import *

# Tableau : la suite d'allumage des cubes
combinaison = []

# Tableau : la suite des choix du joueur
choixJoueur = []

# integer : manche courante
manche = 1
etapeManche = 1

# booleen : indique si quelqu'un a gagné la partie
perdu = False

# booleen : indique si le joueur a tappé un cube
tapote = False

# Cubes
cube1 = 0
cube2 = 0
cube3 = 0


def simon(robot: cozmo.robot.Robot):
    global cube1
    global cube2
    global cube3

    global tapote
    global perdu

    # On récupère les cubes disponibles dans l'environnement du robot
    cube1 = robot.world.get_light_cube(LightCube1Id)  # Cube rouge
    cube2 = robot.world.get_light_cube(LightCube2Id)  # Cube bleu
    cube3 = robot.world.get_light_cube(LightCube3Id)  # Cube vert

    parler(robot, "On va jouer au Simon.")
    parler(robot,
           "Les cubes vont s'allumer dans un certain ordre et tu vas devoir tapper sur les cubes dans le même ordre.")
    parler(robot, "A chaque manche le nombre de cubes qui s'allument augmente. Attention, c'est parti !")

    gen_combinaison()

    handler = robot.add_event_handler(cozmo.objects.EvtObjectTapped, on_cube_tapped)

    for i in range(0, 13):
        jouer_combinaison()

        while (not perdu and etapeManche <= manche + 2):
            if tapote:
                verif_combinaison()
                tapote = False

        if perdu:
            break
        else:
            manche = manche + 1
            etapeManche = 1
            choixJoueur.clear()

    if perdu:
        parler(robot, "Dommage, tu as perdu !")
    else:
        parler(robot, "Bravo, tu as réussi toutes les manches !")


# Génération de la combinaison de cubes
def gen_combinaison():
    global combinaison

    for x in range(15):
        combinaison[x] = random.randint(0, 2)


# Evénement se produisant lorsque le joueur tappe un cube
def on_cube_tapped(event, *, obj, tap_count, tap_duration, **kw):
    global tapote

    # On allume le cube sur lequel le joueur a tappé pendant une seconde
    allumer_cube(obj)

    if obj == cube1:
        choixJoueur.append(0)
    elif obj == cube2:
        choixJoueur.append(1)
    elif obj == cube3:
        choixJoueur.append(2)

    # On met le booleen tapote à true
    tapote = True


# Allumage d'un cube pendant 1 seconde
def allumer_cube(cube):
    global cube1
    global cube2
    global cube3

    if cube == cube1:
        cube1.set_lights(cozmo.lights.red_light)
        time.sleep(1)
        cube1.set_lights(cozmo.lights.off_light)
    elif cube == cube2:
        cube2.set_lights(cozmo.lights.blue_light)
        time.sleep(1)
        cube2.set_lights(cozmo.lights.off_light)
    elif cube == cube3:
        cube3.set_lights(cozmo.lights.green_light)
        time.sleep(1)
        cube3.set_lights(cozmo.lights.off_light)


def jouer_combinaison():
    global manche
    global combinaison

    global cube1
    global cube2
    global cube3

    for x in range(0, manche + 2):
        if combinaison[x] == 0:
            allumer_cube(cube1)
        elif combinaison[x] == 1:
            allumer_cube(cube2)
        elif combinaison[x] == 2:
            allumer_cube(cube3)


def verif_combinaison():
    global perdu
    global etapeManche

    global combinaison
    global choixJoueur

    if choixJoueur[etapeManche] != combinaison[etapeManche]: perdu = True

    if not perdu:
        etapeManche = etapeManche + 1

