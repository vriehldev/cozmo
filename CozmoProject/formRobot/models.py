from django.db import models

class Game(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    file = models.FileField()

    class Meta:
        db_table = "game"

