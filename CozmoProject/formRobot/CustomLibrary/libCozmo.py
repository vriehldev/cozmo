import cozmo
from cozmo import robot

from cozmo.objects import LightCube1Id, LightCube2Id, LightCube3Id
from cozmo.util import degrees

#Avancer
        #Le robot avance d'une certaine distance à une certaine vitesse.
    #Paramètres
        # - distance : distance que le robot parcourt
        # - vitesse : vitesse à laquelle le robot avance
def avancer(robot: cozmo.robot.Robot, distance, vitesse):
    robot.drive_straight(distance, vitesse).wait_for_completed()

#Reculer
        #Le robot recule d'une certaine distance à une certaine vitesse.
    #Paramètres
        # - distance : distance que le robot parcourt
        # - vitesse : vitesse à laquelle le robot avance
def reculer(robot: cozmo.robot.Robot, distance, vitesse):
    robot.drive_straight(-distance, vitesse).wait_for_completed()

#Pivoter à gauche
        #Le robot pivote vers la gauche d'un certain angle.
    #Paramètres
        # - angle : l'angle de pivot du robot
def pivoter_a_gauche(robot: cozmo.robot.Robot, angle):
    robot.turn_in_place(degrees(angle)).wait_for_completed()

#Pivoter à droite
        #Le robot pivote vers la droite d'un certain angle.
    #Paramètres
        # - angle : l'angle de pivot du robot
def pivoter_a_droite(robot: cozmo.robot.Robot, angle):
    robot.turn_in_place(degrees(-angle)).wait_for_completed()

#Parler
        #Le robot récite un texte.
    #Paramètres
        # - texte
def parler(robot: cozmo.robot.Robot, texte):
    robot.say_text(texte).wait_for_completed()

#Soulever un cube
        #Le robot soulève un cube.
    #Paramètres
        # - cube : le cube que le robot soulève
def souleverCube(robot: cozmo.robot.Robot, cube):
    robot.pickup_object(cube, num_retries=3).wait_for_completed()

#Poser un cube
        #Le robot pose ce qu'il porte.
def poserCube(robot: cozmo.robot.Robot):
    robot.place_object_on_ground_here(num_retries=3).wait_for_completed()

#Poser un cube sur un autre
        #Le robot pose ce qu'il porte sur un cube.
    #Paramètres
        # - cube : le cube sur lequel le robot pose ce qu'il porte
def placerSur(robot: cozmo.robot.Robot, cube):
    robot.place_on_object(cube, num_retries=3).wait_for_completed()

#def tapperCube():