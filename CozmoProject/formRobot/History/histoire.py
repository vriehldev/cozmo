import random
import cozmo
from cozmo.objects import LightCube1Id, LightCube2Id, LightCube3Id

from formRobot.CustomLibrary.libCozmo import *

# Cubes
cube1 = 0
cube2 = 0
cube3 = 0

histoireFinie = False
etape = 0


def histoire(robot: cozmo.robot.Robot):
    global cube1
    global cube2
    global cube3

    # On récupère les cubes disponibles dans l'environnement du robot
    cube1 = robot.world.get_light_cube(LightCube1Id)  # Cube rouge --> Ciseau
    cube2 = robot.world.get_light_cube(LightCube2Id)  # Cube bleu  --> Pierre
    cube3 = robot.world.get_light_cube(LightCube3Id)  # Cube vert  --> Feuille

    # On ajoute les couleurs aux cubes
    if cube1 is not None:
        cube1.set_lights(cozmo.lights.red_light)

    if cube2 is not None:
        cube2.set_lights(cozmo.lights.blue_light)

    if cube3 is not None:
        cube3.set_lights(cozmo.lights.green_light)

    # while(not histoireFinie and not quitter):

    # Réveiller le robot: lancer commande "reveillezRobot" la lib custom

    robot.say_text("Welcome to. .Bienvenue sur la planete Mars. Robot Cosmos à vos ordre!").wait_for_completed()
    robot.say_text("Pouvez vous m'ordonner d'avancer avancer, je suis un peu rouillez...").wait_for_completed()

    # Robot rouiller:  Le faire avancer un peu

    # Plus de batterie: Lui faire trouver la batterie(cube 1) → détecter le cube

    # Recharger le robot: Lui  faire soulever la batterie (cube 1)

    # Une alarme danger s’enclenche

    # Le  robot nous  signal qu’il doit aller au terminal

    # Lui faire trouver le terminal 1(cube 2):  Détecter +  aller au cube 2

    # Activation du terminal: en tapant dessus

    # Découvre qu'une météorite approche + le robot lui dit qu’il doivent aller activer le laser pour détruire la météorite

    # Doit chercher le terminal du laser(cube 3): détecter cube 3

    # Doit activer le terminal du laser avec un mot de passe vocal : reconnaissance vocale

    # Message  comme  quoi le terminal a besoin d’une batterie

    # Retrouver la batterie : détecter cube 1 + aller devant lui

    # Retrouver la batterie : détecter cube 1 + aller devant lui

    # Ramener la batterie au terminal du laser (cube 3) : soulever le cube et se déplacer avec

    # Taper sur  le terminal du laser (cube 3) pour faire tourner le laser et l’orienter dans la bonne direction

    # Retourner au 1er terminal pour activer le laser

    # Activer le  laser: tapper sur le cube 2

    # Le laser tire  et détruit l'astéroïde : animation de victoire + félicitations

    # Le robot retourne se coucher de lui même car trop de pression pour lui


cozmo.run_program(histoire)