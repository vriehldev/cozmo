from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('learncoding', views.learncoding, name='learncoding'),
    path('games', views.games, name='games'),
    path('history', views.history, name='history'),
    path('compileEditor', views.compileEditor, name='compileEditor'),

    #Jeux à ajouter
    path('playShifumi', views.playShifumi, name='playShifumi'),
    path('playSimon', views.playSimon, name='playSimon'),
    path('playNumber', views.playNumber, name='playNumber'),





]