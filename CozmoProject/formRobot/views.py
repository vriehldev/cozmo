import sys
import cozmo
from django.shortcuts import render
from formRobot.models import Game

#Importer les jeux ici (les jeux doivent se situer de préférence dans le dossier GameScripts)
from formRobot.GameScripts.shifumi import shifumi
from formRobot.GameScripts.number import findNumber
from formRobot.GameScripts.simon import simon



def index(request):
    return render(request, 'index.html')

def learncoding(request):
    return render(request, 'learncoding.html')

def games(request):
    games = Game.objects.all()
    return render(request, 'games.html', {'games':games})

def history(request):
    return render(request, 'history.html')

def compileEditor(request):
    if request.POST['codeArea'] != "":
        textfile = open("formRobot/temp/program.py", "w+")
        a = textfile.write(request.POST['codeArea'])
        textfile.close()
        saveout = sys.stdout
        fsock = open('formRobot/temp/out.log', 'w')
        sys.stdout = fsock

        try:
            exec(open("formRobot/temp/program.py").read())
        except Exception as exc:
            print(exc)

        sys.stdout = saveout
        fsock.close()
        fsockR = open('formRobot/temp/out.log', 'r')
    return render(request, 'learncoding.html', {'log':fsockR.read()})



#Jeux à ajouter
def playShifumi(request):
    cozmo.run_program(shifumi)
    return render(request, 'games.html')

def playSimon(request):
    cozmo.run_program(simon)
    return render(request, 'games.html')

def playNumber(request):
    cozmo.run_program(findNumber)
    return render(request, 'games.html')





